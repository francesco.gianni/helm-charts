#!/usr/bin/env bash
set -e
COMMAND=$1
CHARTS=($(find . | grep Chart.yaml))
echo "Starting $COMMAND charts"
for chart in "${CHARTS[@]}"
do
  chart=$(dirname $chart)
  echo "Processing chart ${chart}"

  if [[ $COMMAND == "lint" ]]
  then
    echo "Linting chart $chart"
    helm lint $chart
  fi

  if [[ $COMMAND == "release" ]]
  then
    version=$(cat $chart/Chart.yaml | yq .version)
    chart=$(basename $chart)
    echo "Releasing chart $chart with version $version"
    helm package $chart
    PUSH_URL="oci://$CI_REGISTRY/$CI_PROJECT_PATH"
    echo "Pushing $PUSH_URL"
    helm push $chart-$version.tgz $PUSH_URL
  fi
done